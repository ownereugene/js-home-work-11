let passwordCheck = document.getElementById("passwordCheck");
let passwordVerification = document.getElementById("passwordVerification");
let icon1Element = document.getElementById("icon1");
let icon2Element = document.getElementById("icon2");

icon1Element.addEventListener("click", function (event) {
    iCanSeeyou(passwordCheck, icon1Element);
});
icon2Element.addEventListener("click", function (event) {
    iCanSeeyou(passwordVerification, icon2Element);
});


function iCanSeeyou(input, icon) {
    if (input.type === "password") {
        icon.classList.replace("fa-eye-slash", "fa-eye");
        input.type = "text";
}
    else {
        icon.classList.replace("fa-eye", "fa-eye-slash");
        input.type = "password";
    }
}

let buttonElement = document.getElementById("button");
let spanElement = document.createElement("span");
buttonElement.before(spanElement);
buttonElement.addEventListener("click", function (event) {
    event.preventDefault();
    if (passwordCheck.value === passwordVerification.value) {
        alert("You are welcome");
        spanElement.innerText = "";
        passwordCheck.value = "";
        passwordVerification.value = "";
    }
    else {
        spanElement.innerText = "Потрібно ввести однакові значення";
        spanElement.style.color = "red";
    }
})
